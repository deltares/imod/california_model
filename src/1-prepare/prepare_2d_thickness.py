import imod

import geopandas as gpd
from scipy.ndimage import binary_opening, binary_closing, gaussian_filter
import pyproj, rasterio

import numpy as np
import xarray as xr


def nan_gaussian_filter(da, sigma=1.0, truncate=4.0):
    """
    Source
    https://stackoverflow.com/questions/18697532/gaussian-filtering-a-image-with-nan-in-python

    Parameters
    ----------
    da : xr.DataArray
        array to apply gaussian filter to
    sigma : float, optional
        sigma for gaussian filter. The default is 1.0.
    truncate : float, optional
        truncate for gaussian filter. The default is 4.0.

    Returns
    -------
    np.ndarray, gaussian filtered values

    """
    U = da.values

    V = U.copy()
    V[np.isnan(U)] = 0
    VV = gaussian_filter(V, sigma=sigma, truncate=truncate)

    W = 0 * U.copy() + 1
    W[np.isnan(U)] = 0
    WW = gaussian_filter(W, sigma=sigma, truncate=truncate)

    Z = VV / WW

    da.values = Z

    return da

#For some reason, numba looked for this function
#Despite it not being used in this script
#Could be caching error.
#Circumvent this by declaring function here as as well
def low_percentile(values, weights):
    return np.nanpercentile(values, 10)

#%%Path management
path_transmissivity = snakemake.input.path_transmissivity
path_thickness = snakemake.input.path_thickness
path_extent = snakemake.input.path_extent
path_model_grid_2d = snakemake.input.path_model_grid_2d

path_geology_2d = snakemake.output.path_geology_2d

proj_epsg = snakemake.params.proj_epsg

#%%Projections
epsg_lambert = "esri:102004"

#%%Read extent and reproject to lambert
# After clipping, we will project the rasters to WGS84, this is more efficient.
extent = gpd.read_file(path_extent).to_crs(epsg_lambert)

#%%Get bbox
bbox = {
    "x": slice(extent.bounds.loc[0, "minx"], extent.bounds.loc[0, "maxx"]),
    "y": slice(extent.bounds.loc[0, "maxy"], extent.bounds.loc[0, "miny"]),
}

#%%Read rasters
transmissivity = imod.rasterio.open(path_transmissivity).sel(bbox)
thickness = imod.rasterio.open(path_thickness).sel(bbox)

#%%Reproject to wgs84
wkt = pyproj.CRS(epsg_lambert).to_wkt()
# For some reason rasterio CRS does not really support ESRI EPSG codes yet,
# so we have to force it to not use these by removing them from the wkt string
wkt = wkt.replace(',ID["ESRI",102004]', "")
src_crs = rasterio.crs.CRS.from_wkt(wkt)

transmissivity = imod.prepare.reproject(
    transmissivity, src_crs=src_crs, dst_crs=proj_epsg
)
thickness = imod.prepare.reproject(thickness, src_crs=src_crs, dst_crs=proj_epsg)

#%%Regrid to model grid
model_grid = xr.open_dataarray(path_model_grid_2d)

regridder = imod.prepare.Regridder("mean")
transmissivity = regridder.regrid(transmissivity, model_grid)
thickness = regridder.regrid(thickness, model_grid)

#%%Deactivate everything outside model domain
transmissivity = model_grid * transmissivity
thickness = model_grid * thickness

#%%Calc k
k_2d = transmissivity / thickness

#%%Clip off isolated cells and fill small holes
mask = np.invert(np.isnan(k_2d))
mask.values = binary_opening(mask)
mask.values = binary_closing(mask)

#%%Clip edges
mask = mask.where(mask == 1).dropna(dim="x", how="all").dropna(dim="y", how="all")

#%%Smooth and fill gaps
smoothing_sigma = 1.0  # May have to tune this value a bit

thickness = nan_gaussian_filter(thickness, sigma=smoothing_sigma) * mask
k_2d = 10 ** nan_gaussian_filter(np.log10(k_2d), sigma=smoothing_sigma) * mask

#%%Assign attrs
ds = xr.merge(
    [
        d.to_dataset(name=name)
        for d, name in zip([mask, k_2d, thickness], ["mask", "k_2d", "thickness"])
    ]
)
ds.attrs["crs"] = epsg_lambert

#%%Write to file
ds.to_netcdf(path_geology_2d)
