import xarray as xr
import numpy as np

#%%Path management
path_model_grid_3d = snakemake.input.path_model_grid_3d
path_river_grid = snakemake.input.path_river_grid
path_initial_head = snakemake.output.path_initial_head

#%%Read
ds = xr.open_dataset(path_model_grid_3d)
stage_init = xr.open_dataset(path_river_grid)["stage"].isel(time=0).drop("time")

starting_head = stage_init.where(np.isfinite(stage_init), ds["DEM"])
starting_head = starting_head.clip(min=0.0, max=stage_init.max().values)
starting_head = (ds["idomain"] * starting_head).where(ds["idomain"] == 1)

starting_head.to_netcdf(path_initial_head)
