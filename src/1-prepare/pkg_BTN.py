import xarray as xr

# %%Path management
path_model_grid_3d = snakemake.input.path_model_grid_3d
path_initial_salt = snakemake.input.path_initial_salt
path_btn = snakemake.output.path_btn
porosity = snakemake.params.porosity

# %%Read
grid_3d = xr.open_dataset(path_model_grid_3d)
sconc = xr.open_dataarray(path_initial_salt)

# %%Process
icbund = grid_3d["idomain"].rename("icbund")
# TODO: Get C_s from first timeslice of river_grid
sconc = sconc.rename("starting_concentration")

# %%Create dataset
btn = xr.merge([icbund, sconc])
btn["porosity"] = porosity

# %%Write
# Swap dimensions
btn = btn.swap_dims({"z": "layer"})

btn.to_netcdf(path_btn)
