import xarray as xr
import imod
import numpy as np


def map_celltable_to_grid(da, like, celltable):
    """Map a celltable to a grid using numpy indexing.

    Function checks if time is included in the provided DataArray, if so
    a time dimension is broadcasted as well.
    """
    coords = {"y": like.y.values, "x": like.x.values}

    if "time" in da.dims:
        coords["time"] = da["time"].values
        dims = ["time", "y", "x"]
        shape = (da["time"].size, *like.shape)
    else:
        dims = ["y", "x"]
        shape = like.shape

    tmp = np.empty(shape)
    tmp.fill(np.nan)
    tmp[..., celltable["row_index"], celltable["col_index"]] = da.values[
        ..., celltable["FID"]
    ]

    output = xr.DataArray(tmp, coords=coords, dims=dims)

    return output


def rasterrize_mesh(ds_mesh, path_mesh_shp, like):
    """Rasterrize mesh by using a celltable. This requires a path to OGR supported vector file, 
    because it uses GDAL to rasterrize.

    Parameters
    ----------
    ds_mesh : xr.Dataset
        the mesh data to be rasterrized, in a UGRID format.
    path_mesh_shp : str
        path to OGR supported vector file (e.g. a shapefile)
    like : xr.DataArray
        raster to be map dataset to.

    Assumptions
    -----------
    -raster is equidistant
    -dx == dy

    Returns
    -------
    ds_grid : xr.Dataset
        rasterrized dataset

    """
    # Celltable
    dx, _, _, dy, _, _ = imod.util.spatial_reference(like)
    assert(np.ndim(dx) == 0) #We assume now that dx is a scalar
    resolution = int(dx/5) 
    
    celltable = imod.prepare.celltable(path_mesh_shp, "FID", resolution, like, chunksize=1e4)

    ds_mesh = ds_mesh.sel(
        laydim=9
    )  # Modflow concept only considers river interface at bottom of the channel

    # Map and create new dataset
    variables = ["sa1", "s1", "rho", "FlowElem_zcc"]
    ds_grid = xr.merge(
        [
            map_celltable_to_grid(ds_mesh[var], like, celltable).rename(var)
            for var in variables
        ]
    )
    return ds_grid


#%%Path management
paths_river_nc = list(snakemake.input.paths_river_nc)
paths_river_shp = list(snakemake.input.paths_river_shp)
path_geology_2d = snakemake.input.path_geology_2d

path_river_grid = snakemake.output.path_river_grid

#%%Sort files to ensure they are in the same order
paths_river_nc.sort()
paths_river_shp.sort()

#%%Read
mask = xr.open_dataset(path_geology_2d)["mask"]
ds_riv_grid = []

for nc, shp in zip(paths_river_nc, paths_river_shp):
    ds_riv = xr.open_dataset(nc)
    ds_riv_grid.append(rasterrize_mesh(ds_riv, shp, mask))

#%%Merge
try:  # Try to merge without conflicting values
    ds_riv_grid = xr.merge(ds_riv_grid)
except xr.core.merge.MergeError:  # If not use more memory expensive method
    ds_riv_grid = xr.concat(ds_riv_grid, dim="sub").mean("sub")

#%%Rename to names used by River Package
renames = {
    "FlowElem_zcc": "bottom_elevation",
    "s1": "stage",
    "rho": "density",
    "sa1": "concentration",
}

ds_riv_grid = ds_riv_grid.rename(renames)
# TODO: Check with Mick if this is correct
ds_riv_grid["bottom_elevation"] = ds_riv_grid["bottom_elevation"] * -1

#%%Write
ds_riv_grid.to_netcdf(path_river_grid)
