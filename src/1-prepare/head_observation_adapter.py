import imod
import xarray as xr
import pandas as pd
import os

def well_layers(like, elevation):
    inds = imod.select.points_indices(like, z=elevation)["z"].values
    layers = like.coords["layer"].values[inds]
    return layers

#%%Path management
model_grid_3d_nc = snakemake.input.model_grid_3d_nc
dem_nc = snakemake.input.dem_nc
obs_raw = snakemake.input.obs_raw

obs_csv = snakemake.output.obs_csv

model_grid_3d = xr.open_dataset(model_grid_3d_nc)
top = xr.open_dataarray(dem_nc)

## Opening observation data file
obs_df = pd.read_csv(obs_raw)
obs_df["date"] = pd.to_datetime(obs_df["date"].values, format='%d-%m-%Y')

#%% Processing
## Calculating elevation of sampled points
obs_df['top'] = imod.select.points_values(top, x=obs_df['X'], y=obs_df['Y'])
obs_df['z'] = obs_df['top'] - 30.0 #No filter depth indicated in original data, we just assume 30 m below surface.
obs_df['elevation_m'] = obs_df['elevation'] * 0.3048      # from feet to meters

## Assign layer numbers to sampled points
obs_df["layer"] = well_layers(model_grid_3d, obs_df["z"])

#%%Select relevant columns
obs_df = obs_df[["date", "location_i", "X", "Y", "top", "z", "layer", "elevation_m"]]

#%%Combine clusters of wells in the same aquifer
obs_df["location_i"] = obs_df["location_i"].str.replace(r"[ABC]$", "")

#%%Rename to conform to standard
rename_d = {"location_i" : "id",
            "X" : "x",
            "Y" : "y", 
            "elevation_m" : "head"}

obs_df = obs_df.rename(columns = rename_d)

#%%Save
obs_df.to_csv(obs_csv)