import xarray as xr
import pandas as pd

#%%Path management
path_recharge = snakemake.input.path_recharge
path_land_mask = snakemake.input.path_land_mask
config_csv = snakemake.input.scenario_file

path_rch = snakemake.output.path_rch

#%%Read
rate = xr.open_dataarray(path_recharge)
land_mask = xr.open_dataarray(path_land_mask)

config = pd.read_csv(config_csv, index_col=0, squeeze=True)
precipitation_reduction = config["precipitation_reduction"]

#%%Select recharge cells and multiply with precipitation reduction factor
# TODO: Calculate Penman-Monteith evapotranspiration
# FUTURE: Use model of unsaturated zone to better simulate recharge.
rate = rate.where(land_mask) * precipitation_reduction

#%%Create dataset
rch = rate.rename("rate").to_dataset()
rch["concentration"] = 0.0

#%%Save
rch.to_netcdf(path_rch)
