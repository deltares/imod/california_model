"""Convert DEM and a thickness to a 3D grid

Extreme values are clipped from the DEM, to ease model convergence.

"""

import scipy.ndimage.morphology

import numpy as np
import xarray as xr

#%%Path management
path_geology_2d = snakemake.input.path_geology_2d
path_DEM_model = snakemake.input.path_DEM_model
path_model_grid_3d = snakemake.output.path_model_grid_3d

#%%Options
nlay = snakemake.params.nlay
DEM_clipoff_quantile = snakemake.params.DEM_clipoff_quantile

#%%Read
geology_2d = xr.open_dataset(path_geology_2d)
DEM = xr.open_dataarray(path_DEM_model)

#%%Clipoff excessively high values in DEM
DEM = DEM.clip(
    max=DEM.quantile(DEM_clipoff_quantile).values,
    min=DEM.quantile(1 - DEM_clipoff_quantile).values,
)

#%%Calculate hydrogeological basis
hydrogeo_base = DEM - geology_2d["thickness"]

#%%Create 3D Grid
interfaces = np.linspace(DEM.max(), hydrogeo_base.min(), num=nlay + 1)
tops = interfaces[:-1]
bots = interfaces[1:]

dims = ("z", "y", "x")
coords = {
    "z": (tops + bots) / 2,
    "y": DEM.y,
    "x": DEM.x,
    "dz": ("z", tops - bots),
    "layer": ("z", np.arange(tops.size) + 1),
    "zbot": ("z", bots),
    "ztop": ("z", tops),
}
nrow = coords["y"].size
ncol = coords["x"].size
nlay = coords["z"].size
idomain = xr.DataArray(np.ones((nlay, nrow, ncol)), coords, dims)

#%%Get model domain
tolerance = 0.001  # absolute tolerance value (in m)
idomain = idomain.where(
    (idomain.zbot > (hydrogeo_base - tolerance)) & (idomain.ztop < (DEM + tolerance)), 0
)

#%%Remove disconnected active parts of the model domain
active_cells = idomain == 1.0

labeled_array, nlabels = scipy.ndimage.label(active_cells.values)

if nlabels > 100:
    raise ValueError(
        """Very disconnected model domain chosen, 
                     provide a better shapefile"""
    )

labels = np.arange(1, nlabels + 1)
counts = np.array([np.sum(labeled_array == label) for label in labels])
dominant_label = labels[np.argmax(counts)]

idomain = idomain.where((labeled_array == dominant_label), 0.0)

#%%Write
grid_3d = xr.merge(
    [
        d.to_dataset(name=name)
        for d, name in zip(
            [idomain, DEM, hydrogeo_base], ["idomain", "DEM", "hydrogeo_base"]
        )
    ]
)

grid_3d.to_netcdf(path_model_grid_3d)
