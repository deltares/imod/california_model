"""Seperate scenarios from scenarios.csv into individual files.
Convenient for Snakemake to make use of "group" functionality
"""

import pandas as pd
import pathlib
import os

#%%Path management
scenarios_csv = snakemake.input.scenarios_csv
scenario_files = snakemake.output.scenario_files
scenario_nrs = snakemake.params.scenario_nrs

scenario_files = [pathlib.Path(f) for f in scenario_files]
os.makedirs(scenario_files[0].parent, exist_ok=True)

#%%Read
df = pd.read_csv(scenarios_csv, index_col=0)

#%%Write
for nr, f in zip(scenario_nrs, scenario_files):
    df.loc[nr].to_csv(f)
