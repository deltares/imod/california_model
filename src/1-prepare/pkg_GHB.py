""""Convert RIV package to GHB package, improves model robustness, 
but results in grave errors for infilatrating river systems"""
import xarray as xr

path_pkg_riv = snakemake.input.path_pkg_riv
path_pkg_ghb = snakemake.output.path_pkg_ghb

ghb = xr.open_dataset(path_pkg_riv)

ghb = ghb.rename({"stage": "head"})
ghb = ghb.drop("bottom_elevation")

ghb.to_netcdf(path_pkg_ghb)
