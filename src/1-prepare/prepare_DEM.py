import imod
import xarray as xr
from scipy.ndimage import distance_transform_edt
import numpy as np


def fill_nearest_neighbour(data, invalid=None):
    """
    Replace the value of invalid 'data' cells (indicated by 'invalid')
    by the value of the nearest valid data cell

    source: https://stackoverflow.com/questions/5551286/filling-gaps-in-a-numpy-array

    Input:
        data:    numpy array of any dimension
        invalid: a binary array of same shape as 'data'.
                 data value are replaced where invalid is True
                 If None (default), use: invalid  = np.isnan(data)

    Output:
        Return a filled array.
    """
    if invalid is None:
        invalid = np.isnan(data)

    ind = distance_transform_edt(invalid, return_distances=False, return_indices=True)
    return data[tuple(ind)]

def low_percentile(values, weights):
    return np.nanpercentile(values, 10)


#%%Path management
path_DEM = snakemake.input.path_DEM
path_geology_2d = snakemake.input.path_geology_2d

path_DEM_model = snakemake.output.path_DEM_model

proj_epsg = snakemake.params.proj_epsg

#%%Read
dem = imod.rasterio.open(path_DEM)
mask = xr.open_dataset(path_geology_2d)["mask"]

dem_crs = dem.crs

#%%Reproject mask to avoid having to reproject the original dem
mask_r = imod.prepare.reproject(mask, src_crs=proj_epsg, dst_crs=dem_crs)

#%%Clip and regrid
regridder = imod.prepare.Regridder(low_percentile)
dem = regridder.regrid(dem, mask_r)

#%%Reproject
dem = imod.prepare.reproject(dem, src_crs=dem_crs, dst_crs=proj_epsg)
dem = regridder.regrid(dem, mask)  # Ensure grids overlap

#%%Pad
dem.values = fill_nearest_neighbour(dem.values)

#%%Mask
dem = dem * mask

#%%Save
dem.name = "dem"
dem.to_netcdf(path_DEM_model)
