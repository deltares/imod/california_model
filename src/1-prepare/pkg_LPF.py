import xarray as xr

#%%Path management
path_geology_3d = snakemake.input.path_geology_3d
path_lpf = snakemake.output.path_lpf

#%%Read
ds_geo = xr.open_dataset(path_geology_3d)

#%%Set everything correct to add to the model
ds_geo = ds_geo.rename({"kh": "k_horizontal", "kv": "k_vertical"})
ds_geo = ds_geo.swap_dims({"z": "layer"})

#%%Write
ds_geo.to_netcdf(path_lpf)
