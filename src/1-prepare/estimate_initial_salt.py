import xarray as xr
import numpy as np
from scipy.ndimage import distance_transform_edt


def c2dens(c, denselp=0.7142859, dens_ref=1000.0):
    return dens_ref + c * denselp


def ghyb_herz(dem, z, C_f, C_s):
    sea_level = 0.0

    dens_f, dens_s = c2dens(C_f), c2dens(C_s)
    z_interface = sea_level + dens_f / (dens_s - dens_f) * (sea_level - dem)
    below_interface = z <= z_interface

    sconc = xr.where(below_interface, C_s, C_f)
    return sconc


def fill_nearest_neighbour(data, invalid=None):
    """
    Replace the value of invalid 'data' cells (indicated by 'invalid')
    by the value of the nearest valid data cell

    source: https://stackoverflow.com/questions/5551286/filling-gaps-in-a-numpy-array

    Input:
        data:    numpy array of any dimension
        invalid: a binary array of same shape as 'data'.
                 data value are replaced where invalid is True
                 If None (default), use: invalid  = np.isnan(data)

    Output:
        Return a filled array.
    """
    if invalid is None:
        invalid = np.isnan(data)

    ind = distance_transform_edt(invalid, return_distances=False, return_indices=True)
    return data[tuple(ind)]


#%%Path management
#path_initial_head = snakemake.input.path_initial_head
path_model_grid_3d = snakemake.input.path_model_grid_3d
path_river_grid = snakemake.input.path_river_grid
path_initial_salt = snakemake.output.path_initial_salt

#%%Read
#initial_head = xr.open_dataarray(path_initial_head)
#dem = initial_head.max(dim="z")
model_grid_3d = xr.open_dataset(path_model_grid_3d)
dem = model_grid_3d["DEM"]

# For the initial salinities use the DEM together with the initial river concentrations
# to estimate an initial fresh-saline interface with ghyben-herzberg
c_s = xr.open_dataset(path_river_grid).isel(time=0)["concentration"].drop("time")
c_s.values = fill_nearest_neighbour(c_s.values)

#%%Process
sconc = ghyb_herz(dem, model_grid_3d.z, 0.0, c_s).rename("starting_concentration")
sconc = sconc.where(model_grid_3d["idomain"]==1.)

#%%Write
sconc.to_netcdf(path_initial_salt)
