import xarray as xr
import geopandas as gpd
import imod
import numpy as np

path_extent = snakemake.input.path_extent
path_model_grid_2d = snakemake.output.path_model_grid_2d

proj_epsg = snakemake.params.proj_epsg
dcell = snakemake.params.dcell

#%%Read extent
extent = gpd.read_file(path_extent).to_crs(proj_epsg)

#%%Get bbox polygon
bbox = extent.bounds.T[0]

#%%Get bbox for raster
xmin = np.floor(bbox["minx"] / dcell) * dcell + dcell / 2
xmax = np.ceil(bbox["maxx"] / dcell) * dcell - dcell / 2
ymin = np.floor(bbox["miny"] / dcell) * dcell + dcell / 2
ymax = np.ceil(bbox["maxy"] / dcell) * dcell - dcell / 2

#%%Discretize 2D
end = dcell / 100  # Value so that np.arange ends at stop not before stop

x = np.arange(xmin, xmax + end, dcell)
y = np.arange(ymax, ymin - end, -dcell)

#%%Create DataArray
model_grid = xr.DataArray(1, coords={"x": x, "y": y}, dims=["y", "x"])

#%%Rasterrize
model_grid = imod.prepare.spatial.rasterize(extent, model_grid)
model_grid.attrs["crs"] = proj_epsg

#%%Write netcdf
model_grid.to_netcdf(path_model_grid_2d)
