# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 15:25:42 2020

@author: engelen
"""
import xarray as xr
import pandas as pd

#%%Path management
path_model_grid_3d = snakemake.input.path_model_grid_3d
path_geology_2d = snakemake.input.path_geology_2d
config_csv = snakemake.input.scenario_file

path_geology_3d = snakemake.output.path_geology_3d

#%%Read
k_2d = xr.open_dataset(path_geology_2d)["k_2d"]
idomain = xr.open_dataset(path_model_grid_3d)["idomain"]

config = pd.read_csv(config_csv, index_col=0, squeeze=True)
anisotropy = config["anisotropy"]
kh_multiplication = config["kh_multiplication"]

#%%Geology go for homogeneous model now
kh = idomain.where(idomain == 1) * k_2d * kh_multiplication
kv = kh / anisotropy

#%%Write
geology_3d = xr.merge(
    [d.to_dataset(name=name) for d, name in zip([kh, kv], ["kh", "kv"])]
)

geology_3d.to_netcdf(path_geology_3d)
