import xarray as xr

# %%Path management
path_model_grid_3d = snakemake.input.path_model_grid_3d
path_initial_head = snakemake.input.path_initial_head
path_bas = snakemake.output.path_bas

# %%Read
ds = xr.open_dataset(path_model_grid_3d)

ds = ds.rename({"idomain": "ibound"})

starting_head = xr.open_dataarray(path_initial_head)

# Estimate starting head
ds["starting_head"] = starting_head

# Swap dimensions because layer
ds = ds.swap_dims({"z": "layer"})

ds["top"] = ds.coords["ztop"].sel(layer=1)
ds["bottom"] = ds.coords["zbot"]

ds = ds.drop_vars(["DEM", "hydrogeo_base"])

ds.close()

ds.to_netcdf(path_bas)
