"""Script to repeat one year of simulations N times for rivers, 
depending on the model times (right now gotten from recharge)

Script is capable of finding the closest matches of river times to model times
(inexact lookup) and repeat these river times until the entire model time 
extent is filled.

t_mod       |------------------------------|
t_riv          |-----|

t_riv_new   ---|-----|-----|-----|-----|---- 

"""

import xarray as xr
import numpy as np


def year(dates):
    """Return an array of the years given an array of datetime64s"
    Source = https://stackoverflow.com/questions/31407463/numpy-and-pandas-timedelta-error

    Note: only works for numpy arrays, does not work for xarray datetime arrays."""
    return dates.astype("M8[Y]").astype("i8") + 1970


def month(dates):
    """Return an array of the months given an array of datetime64s
    Source = https://stackoverflow.com/questions/31407463/numpy-and-pandas-timedelta-error

    Note: only works for numpy arrays, does not work for xarray datetime arrays.
    """
    return dates.astype("M8[M]").astype("i8") % 12 + 1


def calc_years_offset(t1, t2):
    """Calculate offset in years from two datetime64 arrays"""
    years_t1 = year(t1)
    years_t2 = year(t2)
    year_offset = years_t2[0] - years_t1[0] + 1
    return year_offset


#%%Path management
path_river = snakemake.input.path_river
path_time_model = snakemake.input.path_time_model

dt_forcing = snakemake.params.dt_forcing

path_river_nyears = snakemake.output.path_river_nyears

#%%Read
ds_riv = xr.open_dataset(path_river)
time_model = xr.open_dataset(path_time_model)["time"]

#%%Time management
n_triv = ds_riv.time.size
n_tmod = time_model.size

n_stress_periods = int(365 / dt_forcing)
n_repeats = int(n_tmod / n_stress_periods)

year_offset = calc_years_offset(ds_riv.time.values, time_model.values)

# Offset river times by an amount of year_offset
time_riv_offset = ds_riv.time + np.timedelta64(year_offset * 365, "D")
time_riv_offset = time_riv_offset.assign_coords(time=time_riv_offset.values)

# Select dates closest to the offset river times in the model times
new_dates_riv = time_model.sel(time=time_riv_offset.values, method="nearest")
new_dates_riv = new_dates_riv.isel(time=slice(0, n_stress_periods))

# Find the corresponding index
id_start = np.squeeze(np.argwhere((time_model == new_dates_riv[0]).values))

# Create list of repeated datasets
ds_ls = []

# Select initial bit of ds_riv
start_ts = time_model[0:id_start]
ds_ls.append(
    ds_riv.isel(time=slice(n_triv - id_start, None)).assign_coords(time=start_ts)
)

# Repeatly add ds_riv
for i in range(n_repeats):
    start = i * n_triv + id_start
    end = (i + 1) * n_triv + id_start
    mid_ts = time_model.isel(time=slice(start, end))
    ds_ls.append(
        ds_riv.isel(
            time=slice(
                0, mid_ts.size
            )  # Select with this slice, so that end can be shorter than n_triv.
        ).assign_coords(time=mid_ts)
    )

ds_riv_nyears = xr.concat(ds_ls, dim="time")

#%%Test
assert np.all(ds_riv_nyears.time == time_model)

#%%Write to netcdf
ds_riv_nyears.to_netcdf(path_river_nyears)
