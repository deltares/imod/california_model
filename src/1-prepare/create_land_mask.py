import imod
import xarray as xr
import numpy as np

#%%Path management
path_model_grid_3d = snakemake.input.path_model_grid_3d
path_river_grid = snakemake.input.path_river_grid
path_land_mask = snakemake.output.path_land_mask

#%%Read
mask = xr.open_dataset(path_model_grid_3d)["idomain"].max(dim="z")

river_active = np.isfinite(xr.open_dataset(path_river_grid).isel(time=0)["stage"])

#%%Determine land mask
land_mask = (mask == 1) & ~river_active

#%%Write
land_mask.to_netcdf(path_land_mask)
