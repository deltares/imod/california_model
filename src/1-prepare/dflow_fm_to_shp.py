"""Convert flexible mesh to shapefile with polygons
"""

import numpy as np
import xarray as xr

import pygeos
import geopandas as gpd
import pandas as pd
import os


def _fixed_mesh_to_polygon(ds_sel, nodes=[0, 1, 2, 3]):
    ds_sel = ds_sel.sel(nFlowElemContourPts=nodes)

    face_edges_sel = pygeos.creation.linearrings(
        ds_sel["FlowElemContour_x"], y=ds_sel["FlowElemContour_y"]
    )
    faces_sel = pygeos.creation.polygons(face_edges_sel)
    return faces_sel


def flexible_mesh_to_gdf(ds, crs, i):
    """Convert flexible mesh saved in UGRID to GeoDataFrame

    First splits up dataset into triangles and quadrilaterals,
    seperately converts them to geodataframe and concatenates.

    """

    # Split up the triangles and rectangles to avoid PyGeos putting NaNs in Polygon geometries
    # Indexes of rectangles
    id_quad = (
        ds["FlowElemContour_x"].dropna(dim="nFlowElem", how="any")["nFlowElem"].values
    )
    id_tri = ds.nFlowElem.values[~np.isin(ds.nFlowElem.values, id_quad)]

    # PyGeos does not accept staggered grids yet therefore we create two seperate
    # linearring arrays and concatenate them later
    faces_quad = _fixed_mesh_to_polygon(ds.sel(nFlowElem=id_quad), nodes=[0, 1, 2, 3])
    gdf_quad = gpd.GeoDataFrame(index=id_quad, geometry=faces_quad, crs=crs)

    if len(id_tri) > 0:
        faces_tri = _fixed_mesh_to_polygon(ds.sel(nFlowElem=id_tri), nodes=[0, 1, 2])
        gdf_tri = gpd.GeoDataFrame(index=id_tri, geometry=faces_tri, crs=crs)
    else:
        # No triangles present
        gdf_tri = gpd.GeoDataFrame()

    gdf = pd.concat([gdf_quad, gdf_tri]).sort_index()
    return gdf


#%%Path management
paths_DFM = snakemake.input.paths_DFM
paths_river_shp = snakemake.output.paths_river_shp
proj_epsg = snakemake.params.proj_epsg

#%%Create output folder
os.makedirs(os.path.dirname(paths_river_shp[0]), exist_ok=True)

#%%Read
ds_ls = [xr.open_dataset(p).isel(time=0) for p in paths_DFM]

# MetaData wrong in Netcdf
# epsg_code = ds["projected_coordinate_system"].attrs["epsg"]
# crs = f"epsg:{epsg_code}"
crs = "EPSG:26910"

#%%Assign ids as coordinate for bookkeeping
ds_ls = [ds.assign_coords(nFlowElem=(ds["nFlowElem"].values)) for ds in ds_ls]

#%%Create GeoDataFames
gdf_ls = [flexible_mesh_to_gdf(ds, crs, i) for i, ds in enumerate(ds_ls)]

#%%Reproject
gdf_ls = [gdf.to_crs(proj_epsg) for gdf in gdf_ls]

#%%Save to shapefile
for gdf, path in zip(gdf_ls, paths_river_shp):
    gdf.to_file(path)

#%%Create overall shapefile for overview
gdf_tot = pd.concat(gdf_ls, keys=range(len(gdf_ls))).reset_index()
gdf_tot.columns = ["nc_id", "el_id", "geometry"]

gdf_tot.to_file(os.path.join(paths_river_shp[0], "..", "river_shape_tot.shp"))
