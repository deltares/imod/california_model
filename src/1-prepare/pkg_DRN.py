import xarray as xr
import numpy as np
import pandas as pd

#%%Path management
path_land_mask = snakemake.input.path_land_mask
path_model_grid_3d = snakemake.input.path_model_grid_3d
config_csv = snakemake.input.scenario_file

path_pkg_drn = snakemake.output.path_pkg_drn

dcell = snakemake.params.dcell

#%%Read
land_mask = xr.open_dataarray(path_land_mask)
ds_grid = xr.open_dataset(path_model_grid_3d)
dem = ds_grid["DEM"]

config = pd.read_csv(config_csv, index_col=0, squeeze=True)
resistance = config["resistance_drain"]

#%%Calculate location of cells
active = ds_grid["idomain"].where(ds_grid["idomain"] == 1.0)
land_surface = dem.where(land_mask).clip(min=0.0)  # Drains not allowed to go below 0 m

top_grid = (active * ds_grid.z).max(dim="z")

drn_cells = xr.where(ds_grid.z == top_grid, land_surface, np.nan)

drain_elevation = drn_cells
drain_elevation = drain_elevation.transpose("z", "y", "x")

# Calculate donductance
conductance = dcell ** 2 / resistance

# Create dataset
drn = drain_elevation.rename("elevation").to_dataset()
drn["conductance"] = xr.where(np.isfinite(drn["elevation"]), conductance, np.nan)

# Swap dimensions for the save and drop time
drn = drn.swap_dims({"z": "layer"}).drop("time")

#%%Save
drn.to_netcdf(path_pkg_drn)
