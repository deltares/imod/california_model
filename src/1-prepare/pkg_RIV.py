import pandas as pd
import xarray as xr
import numpy as np


#%%Path management
path_river = snakemake.input.path_river
path_model_grid_3d = snakemake.input.path_model_grid_3d
config_csv = snakemake.input.scenario_file

path_pkg_riv = snakemake.output.path_pkg_riv

dcell = snakemake.params.dcell

# TODO Find way to compress this, remove bottom layers for example that are inactive or save to zarr.

#%%Read
ds_riv = xr.open_dataset(path_river)
ds_grid = xr.open_dataset(path_model_grid_3d)

config = pd.read_csv(config_csv, index_col=0, squeeze=True)

resistance = config["resistance_river"]

#%%Assign concuctance
conductance = dcell ** 2 / resistance

ds_riv["conductance"] = xr.where(
    np.isfinite(ds_riv["concentration"]), conductance, np.nan
)

#%%Find location of river cells
riv_cells = xr.where(
    (ds_riv["bottom_elevation"] > ds_grid.zbot)
    & (ds_riv["bottom_elevation"] < ds_grid.ztop),
    1,
    np.nan,
)

riv_cells = riv_cells.transpose("time", "z", "y", "x")

#%%Broadcast to 4D Dataset
ds_riv = riv_cells * ds_riv

#%%Trim inactive layers
ds_riv = ds_riv.dropna(dim="z", how="all")

#%%Swap dims
ds_riv = ds_riv.swap_dims({"z": "layer"})

#%%Write
ds_riv.to_netcdf(path_pkg_riv)
