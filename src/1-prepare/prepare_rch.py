import xarray as xr
import pyproj
from scipy.interpolate import NearestNDInterpolator

import numpy as np


def assign_dcoords(da, dim):
    dcoords = np.diff(da[dim].values)
    dcoords = np.insert(dcoords, 0, dcoords[0])
    ddim = "d" + dim
    da = da.assign_coords({ddim: (dim, dcoords)})
    return da


def reproject_nearest_neighbour(da, lon, lat, proj_epsg, like):
    """Reproject data on a non-equidistant WGS84 grid of lats and lons
    to a equidistant raster with a 'proj_epsg' projection, with a nearest
    neighbour lookup.
    """
    # Transform coordinates from WGS84 to proj_epsg
    src_crs = "epsg:4326"
    transformer = pyproj.Transformer.from_crs(src_crs, proj_epsg, always_xy=True)
    shape = lon.shape
    xx = lon.values.ravel()
    yy = lat.values.ravel()
    xnew, ynew = map(lambda x: x.reshape(shape), transformer.transform(xx=xx, yy=yy))

    # Create indexer
    indexer = np.arange(xnew.size).reshape(shape)

    # Create interpolator
    coords = np.vstack([xnew.ravel(), ynew.ravel()]).transpose()
    interpolator = NearestNDInterpolator(coords, indexer.ravel())
    yy, xx = np.meshgrid(like["y"].values, like["x"].values, indexing="ij")

    indexer = interpolator(xx, yy)
    resampled = da.values.reshape(da.time.size, -1)[:, indexer]

    coords = {
        "x": like.coords["x"].values,
        "y": like.coords["y"].values,
        "time": da.coords["time"].values,
    }

    reprojected = xr.DataArray(resampled, coords=coords, dims=["time", "y", "x"])

    return reprojected


#%%Path management
paths_prcp = list(snakemake.input.paths_prcp)
path_geology_2d = snakemake.input.path_geology_2d
path_recharge = snakemake.output.path_recharge

proj_epsg = snakemake.params.proj_epsg
dt_forcing = snakemake.params.dt_forcing

#%%Read
paths_prcp.sort()

ds = xr.open_mfdataset(paths_prcp, data_vars="minimal")
mask = xr.open_dataset(path_geology_2d)["mask"]

#%%Resample to weekly
prec = ds["total_precipitation_12-hour"].resample(time=f"{dt_forcing}D").sum().compute()
prec = prec / dt_forcing  # Convert mm/Nd to mm/d
prec = prec / 1000.0  # Convert mm/d to m/d

prec_reprojected = reproject_nearest_neighbour(
    prec, ds["lon"], ds["lat"], proj_epsg, mask
)

#%%Calculate recharge
rch = prec_reprojected * mask

#%%Write to file
rch.to_netcdf(path_recharge)
