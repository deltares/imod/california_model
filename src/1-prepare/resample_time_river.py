import xarray as xr

#%%Path management
paths_DFM = list(snakemake.input.paths_DFM)
paths_river_nc = list(snakemake.output.paths_river_nc)
dt_forcing = snakemake.params.dt_forcing

paths_DFM.sort()
paths_river_nc.sort()

#%%Read and resample
ds_resampled = [
    xr.open_dataset(p).resample(time=f"{dt_forcing}D").mean() for p in paths_DFM
]

#%%Write
for ds, path in zip(ds_resampled, paths_river_nc):
    ds.to_netcdf(path)
