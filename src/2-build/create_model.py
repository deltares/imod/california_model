import os

import imod
import pandas as pd
import xarray as xr


def pkg_from_file(pkg, path):
    """
    Open package from file,
    """
    return pkg(**xr.open_dataset(path))


# %%Inputs
path_bas = snakemake.input.path_bas
path_btn = snakemake.input.path_btn
path_lpf = snakemake.input.path_lpf
path_drn = snakemake.input.path_drn
path_riv = snakemake.input.path_riv
path_ghb = snakemake.input.path_ghb
path_rch = snakemake.input.path_rch

path_runf = snakemake.output.path_runf

surface_water_concept = snakemake.params.surface_water_concept

modelname = snakemake.params.modelname
longitudinal_dispersivity = snakemake.params.longitudinal_dispersivity
dcell = snakemake.params.dcell

hclose = snakemake.params.hclose
solver = snakemake.params.solver

proj_epsg = snakemake.params.proj_epsg
scenario_nr = snakemake.params.scenario_nr

# %%Path management
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

model_dir = os.path.dirname(path_runf)
result_dir = os.path.join(model_dir, "..", "4-output")

# %%Calculate
rclose = hclose * dcell**2 * 1000

# %%Create model
m = imod.wq.SeawatModel(modelname)

# Geometry
m["bas"] = pkg_from_file(imod.wq.BasicFlow, path_bas)
m["btn"] = pkg_from_file(imod.wq.BasicTransport, path_btn)
m["lpf"] = pkg_from_file(imod.wq.LayerPropertyFlow, path_lpf)
m["lpf"]["save_budget"] = True

# Boundary conditions
m["drn"] = pkg_from_file(imod.wq.Drainage, path_drn)
m["drn"]["save_budget"] = True

if surface_water_concept == "RIV":
    m["riv"] = pkg_from_file(imod.wq.River, path_riv)
    m["riv"]["save_budget"] = True
elif surface_water_concept == "GHB":
    m["ghb"] = pkg_from_file(imod.wq.GeneralHeadBoundary, path_ghb)
    m["ghb"]["save_budget"] = True
else:
    raise ValueError("surface_water_concept should be either RIV or GHB")

m["rch"] = pkg_from_file(imod.wq.RechargeHighestActive, path_rch)
m["rch"]["save_budget"] = True

# Solute transport settings
m["adv"] = imod.wq.AdvectionTVD()
m["dsp"] = imod.wq.Dispersion(
    longitudinal=longitudinal_dispersivity, ratio_vertical=0.01
)  # For some reason imod-python has high default for this
m["vdf"] = imod.wq.VariableDensityFlow(density_concentration_slope=0.7143)

# Output Control
m["oc"] = imod.wq.OutputControl(
    save_head_idf=True,
    save_concentration_idf=True,
    save_budget_idf=True,
)

if solver == "pcg":
    m["pcg"] = imod.wq.PreconditionedConjugateGradientSolver(
        hclose=hclose, rclose=rclose
    )
elif solver == "pksf":
    # Solvers
    m["pksf"] = imod.wq.ParallelKrylovFlowSolver(
        hclose=hclose, rclose=rclose, h_fstrict=1.0, r_fstrict=1.0, partition="rcb"
    )
else:
    raise ValueError("Solver should either be 'pcg' or 'pksf'")

m["pkst"] = imod.wq.ParallelKrylovTransportSolver(cclose=1.0e-6, partition="rcb")

# Assume river package is in the correct time discretization
model_times = pd.DatetimeIndex(m["rch"]["time"].values)

m.time_discretization(model_times)
m["time_discretization"]["n_timesteps"] = 7
m["time_discretization"]["timestep_multiplier"] = 1.5
m["time_discretization"]["transport_timestep_multiplier"] = 1.5

m.write(
    f"data/3-input/scenario_{scenario_nr}",
    result_dir=f"data/4-output/scenario_{scenario_nr}",
    resultdir_is_workdir=True,
)

# %%Write Qgis project
# m.write_qgis_project(f"data/3-input/scenario_{scenario_nr}/qgis_project",
#                     crs=proj_epsg, aggregate_layers=True)
