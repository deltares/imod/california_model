# Importing packages
import imod, tqdm, os
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import pathlib
import seaborn as sns


#%%Seaborn settings
scaling_factor = 2.0

sns.set(font_scale = scaling_factor)
sns.set_style("whitegrid")
#%% Input
head_nc = snakemake.input.head_nc
obs_csv = snakemake.input.obs_csv
path = snakemake.output.figure_dir

#%% Create output folder
path = pathlib.Path(path)
os.makedirs(path, exist_ok=True)

#%% Setting environment
## Opening data arrays
head = xr.open_dataarray(head_nc)
obs_df = pd.read_csv(obs_csv, index_col=0)
obs_df["time"] = pd.to_datetime(obs_df["date"].values, format='%Y-%m-%d')

## Select modeldata (sim) at sampling points
sim_data = []
for well_id, well_df in tqdm.tqdm(obs_df.groupby("id")):
    x = well_df["x"].iloc[0]
    y = well_df["y"].iloc[0]
    layer = well_df["layer"].iloc[0]
    arrayhead = imod.select.points_values(head, x=x, y=y, layer=layer)
    df_temp = pd.DataFrame()
    df_temp["time"] = arrayhead.time
    df_temp["head"] = arrayhead.values
    df_temp["id"] = well_id
    sim_data.append(df_temp)

sim_data = pd.concat(sim_data)
sim_data["time"] = pd.to_datetime(sim_data["time"].values)
sim_data = sim_data.where(sim_data["head"]<10000000).dropna(axis=0, how="all")

#%%Create dataframe in long format
obs_df = obs_df.loc[:, ["time", "id", "head"]]
obs_df["type"] = "measured"

sim_data["type"] = "modelled"

df = pd.concat([obs_df, sim_data])

#%% Plotting
stations = obs_df["id"].unique()
for well_id, well_df in (df.groupby("id")):
    
    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    sns.lineplot(data = well_df, x="time", y = "head", 
                 hue = "type", palette = "dark:seagreen",
                 err_style = "bars", 
                 err_kws = dict(capsize = 3, capthick= 1.5*scaling_factor),
                 linewidth=scaling_factor)
    plt.title(well_id)
    plt.tight_layout()
    plt.savefig(path / (well_id + '_comparison.png'))
    plt.close()