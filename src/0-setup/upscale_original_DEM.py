#%% Imports
import imod
import xarray as xr
import numpy as np
import os

def low_percentile(values, weights):
    return np.nanpercentile(values, 10)


#%% Path management
# For interactive runs
#path_DEM_original = pathlib.Path("../../data/1-external/DEM/sfbaydeltadem10m2016.asc")
#path_DEM_upscaled = pathlib.Path("../../data/1-external/DEM_100m/sfbaydeltadem100m2016.asc")

# For snakemake runs
path_DEM_original = snakemake.input.path_DEM_original
path_DEM_upscaled = snakemake.output.path_DEM_upscaled

#%% Read
dem = imod.rasterio.open(path_DEM_original)

#%% Create upscaled grid
n_step = 10
start = int(n_step/2)

x_up = dem.x.values[start::n_step]
y_up = dem.y.values[start::n_step]

dx_up = dem.dx.values * n_step
dy_up = dem.dy.values * n_step

shape = (y_up.size, x_up.size)

like = xr.DataArray(np.empty(shape), coords={"y" : y_up, "x" : x_up}, dims=("y", "x"))
like = like.assign_coords(dx = dx_up, dy=dy_up)

#%% Clip and regrid
regridder = imod.prepare.Regridder(low_percentile)
dem_up = regridder.regrid(dem, like)

#%% Set CRS
dem_up.attrs["crs"] = dem.attrs["crs"]

#%% Write
# rasterio write function does not automatically create dictionary
os.makedirs(os.path.dirname(path_DEM_upscaled), exist_ok=True)

# write
imod.rasterio.write(path_DEM_upscaled, dem_up)

# %%
