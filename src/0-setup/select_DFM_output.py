import xarray as xr

#%%Path management
# NOTE! Reading data from the P: drive is very slow, better to run this part on a Horizon node
paths_DFM = snakemake.input.paths_DFM
paths_DFM_sel = snakemake.output.paths_DFM_sel

# from glob import glob
# glob_path = r"c:\Users\engelen\California_Bay\DFM_OUTPUT_r12\r12_*_map.nc"
# path_format = r"c:\Users\engelen\California_Bay\DFM_SELECT\r12_{:04d}_map.nc"

# paths_DFM = glob(glob_path)


#%%Read and select variables
var_select = [
    "rho",  # Density
    "s1",  # water level with respect to mean sea level
    "sa1",  # Salinity (% sea water?)
    "FlowElemContour_x",  # x values nodes of face
    "FlowElemContour_y",  # y values nodes of face
    "FlowElem_zcc",  # bed level river
    "FlowElem_bac",  # area of face
    "Mesh2D",  # meta info on mesh topology
    "projected_coordinate_system",  # Projection, see attrs
]

ds_ls = [xr.open_dataset(path)[var_select] for path in paths_DFM]

#%%Write
for path_DFM_sel, ds in zip(paths_DFM_sel, ds_ls):
    ds.to_netcdf(path_DFM_sel)
