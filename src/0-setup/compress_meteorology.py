#%% Import
import xarray as xr
import pathlib
from glob import glob
import os

#%% Path management
## Interactive run
#inpdir = pathlib.Path("../../data/1-external/meteoric_forcing").resolve()
#outdir = (inpdir / ".." / "meteoric_compressed").resolve()
#
#paths_prcp = glob(str(inpdir / "*.nc"))
#paths_prcp.sort()
#paths_prcp_comp = [outdir / os.path.basename(path) for path in paths_prcp]

## Snakemake run
paths_prcp      = list(snakemake.input.paths_DFM_sel)
paths_prcp_comp = snakemake.output.paths_DFM_comp

paths_prcp.sort()

#%% Read
ds_ls = [xr.open_dataset(path) for path in paths_prcp]

# %% Write and compress
os.makedirs(os.path.dirname(paths_prcp_comp[0]), exist_ok=True)

vars = ["total_precipitation_12-hour"]
# complevel 1 already delivers enough compression in this case. 
# level 9 creates too much computational overhead with little storage gain.
encoding = dict([(var, {"zlib":True, "complevel": 1}) for var in vars])

for path, ds in zip(paths_prcp_comp, ds_ls):
    ds.to_netcdf(path, engine = "netcdf4", encoding = encoding)
