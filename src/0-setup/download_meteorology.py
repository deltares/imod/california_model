# -*- coding: utf-8 -*-
"""
Created on Fri Sep 25 12:56:29 2020

@author: engelen
"""

import requests
import shutil


def download_file(url, path_out):
    # local_filename = url.split('/')[-1]
    with requests.get(url, stream=True) as r:
        with open(path_out, "wb") as f:
            shutil.copyfileobj(r.raw, f)


#%%Path management
times = snakemake.params.time_strings
paths_prcp = snakemake.output.paths_prcp

#%%Download
url_template = "http://thredds.cencoos.org/thredds/ncss/COAMPS_4KM_TTL_PRCP.nc?var=total_precipitation_12-hour&north=40&west=-125&east=-120&south=36&horizStride=1&horizStride=1&time_start={}T00%3A00%3A00Z&time_end={}T00%3A00%3A00Z&timeStride=2&addLatLon=true&accept=netcdf"

# WARNING: Note that size of these files should not exceed 500MB, otherwise ncss blocks request
for startstr, endstr, path in zip(times[:-1], times[1:], paths_prcp):
    download_file(url_template.format(startstr, endstr), path)
