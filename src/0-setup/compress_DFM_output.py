#%% Import
import xarray as xr
import numpy as np
import pathlib
from glob import glob
import os

def compress_variables(ds):
    vars = ["rho", "s1", "sa1"]
    for var in vars:
        ds[var] = ds[var].astype(np.float32, casting="same_kind")

    return ds

#%% Path management
## Interactive run
#inpdir = pathlib.Path("../../data/1-external/DFM_Select").resolve()
#outdir = (inpdir / ".." / "DFM_compressed").resolve()
#
#dfm_paths = glob(str(inpdir / "*.nc"))
#dfm_paths.sort()
#dfm_compressed_paths = [outdir / "r12_{:04d}_map.nc".format(i) for i in range(len(dfm_paths))]

## Snakemake run
dfm_paths               = list(snakemake.input.paths_DFM_sel)
dfm_compressed_paths    = snakemake.output.paths_DFM_comp

dfm_paths.sort()

#%% Read
ds_ls = [xr.open_dataset(path) for path in dfm_paths]

# %% Write and compress
os.makedirs(os.path.dirname(dfm_compressed_paths[0]), exist_ok=True)

vars = ["rho", "s1", "sa1"]
# complevel 1 already delivers enough compression in this case. 
# level 9 creates too much computational overhead with little storage gain.
encoding = dict([(var, {"zlib":True, "complevel": 1}) for var in vars])

for path, ds in zip(dfm_compressed_paths, ds_ls):
    ds = compress_variables(ds)
    ds.to_netcdf(path, engine = "netcdf4", encoding = encoding)

# %%
