import os

import imod
import pyvista as pv
import xarray as xr

# %%Path management
head_nc = snakemake.input.head_nc
head_png = snakemake.output.head_png

# %%Read
head = xr.open_dataarray(head_nc)

# %%Prepare for plot
head = head.swap_dims({"layer": "z"})

head_plot = head.isel(time=-1).clip(min=0.0)

scale_vertical = (head.dy / head.dz).values

# %%Convert to pyvista 3D grid.
grid_3d = imod.visualize.pyvista.grid_3d(
    head_plot, exterior_only=False, vertical_exaggeration=scale_vertical
)

grid_3d = grid_3d.clip("x")

# %%Settings
sargs = dict(
    title_font_size=30,
    label_font_size=24,
    shadow=True,
    n_labels=8,
    fmt="%.0f",
    font_family="arial",
    position_x=0.05,
    position_y=0.05,
    title="Head (m)",
)

# %%Plot
pv.set_plot_theme("dark")

plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(grid_3d, cmap="viridis", lighting=True, scalar_bar_args=sargs)
plotter.show_grid()

plotter.show(screenshot=head_png)
# %%
