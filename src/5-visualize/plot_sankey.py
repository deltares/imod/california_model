import numpy as np
import imod
import xarray as xr
import os
import matplotlib.pyplot as plt
from matplotlib.sankey import Sankey


def split_bdg(ds, key):
    """Split budgets in a budget in and a budget out"""
    ds[f"{key}_out"] = ds[f"{key}"].where(ds[f"{key}"] < 0)
    ds[f"{key}_in"] = ds[f"{key}"].where(ds[f"{key}"] > 0)
    ds = ds.drop(f"{key}")
    return ds


#%%Path management
bdg_nc = snakemake.input.bdg_nc
sankey_png = snakemake.output.sankey_png

#%%Read
ds = xr.open_dataset(bdg_nc)
ds = ds.isel(time=-1)  # for last timestep

#%%Split budgets in positive and negative flux
for pkg in ["bdgriv", "bdgghb", "bdgsto"]:
    if pkg in ds.keys():
        ds = split_bdg(ds, pkg)

#%%For some versions of imod-python spatial ref needs to be removed
if "spatial_ref" in ds.keys():
    ds = ds.drop("spatial_ref")

#%%Prepare for Sankey
d_labels = {
    "riv_in": "river in",
    "riv_out": "river out",
    "ghb_in": "bay/sea in",
    "ghb_out": "bay/sea out",
    "sto_in": "storage in",
    "sto_out": "storage out",
    "drn": "Dunnian runoff",
    "rch": "infiltration",
}

ds_sum = ds.sum()
d_bdg = ds_sum.to_dict()["data_vars"]
labels = [d_labels[key.split("bdg")[1]] for key in ds.keys()]
flows = [d_bdg[key]["data"] for key in ds.keys()]
labels, flows = zip(
    *[(label, flow) for label, flow in zip(labels, flows) if not np.isclose(flow, 0.0)]
)

n = int(np.ceil(len(flows) / 3))
orientations = np.tile([1, -1, 0], n)[: len(flows)]


#%%Plot Sankey
plt.rcParams.update({"font.size": 14})

fig, ax = plt.subplots(figsize=(10, 8))

sankey = Sankey(
    ax=ax,
    flows=flows,
    orientations=orientations,
    labels=labels,
    scale=1 / max(flows) * 0.4,
    unit=r" $m^3/d$",
    pathlengths=0.3,
    offset=0.25,
    format="%.1e",
).finish()
plt.axis("off")

plt.tight_layout()
plt.savefig(sankey_png, dpi=300)
