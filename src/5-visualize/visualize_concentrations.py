import os

import imod
import matplotlib.pyplot as plt
import pyvista as pv
import xarray as xr

#%%Path management
conc_nc = snakemake.input.conc_nc
conc_png = snakemake.output.conc_png

#%%Read
conc = xr.open_dataarray(conc_nc)

#%%Prepare for plot
conc = conc.swap_dims({"layer": "z"})

conc_plot = conc.isel(time=-1).clip(min=0.0)

scale_vertical = (conc.dy / conc.dz).values

#%%Convert to pyvista 3D grid.
grid_3d = imod.visualize.pyvista.grid_3d(
    conc_plot, exterior_only=False, vertical_exaggeration=scale_vertical
)

grid_3d = grid_3d.clip("x")

#%%Settings
sargs = dict(
    title_font_size=30,
    label_font_size=24,
    shadow=True,
    n_labels=8,
    fmt="%.0f",
    font_family="arial",
    position_x=0.05,
    position_y=0.05,
    title="Salinity (TDS)",
)

#%%Plot
pv.set_plot_theme("dark")

cmap = plt.cm.get_cmap("RdYlBu_r", 35)

plotter = pv.Plotter(off_screen=True)
plotter.add_mesh(grid_3d, cmap=cmap, lighting=True, clim=[0, 35], scalar_bar_args=sargs)

# FUTURE: Hopefully PyVista comes up with a way to set font sizes,
# right now it does nothing due to a VTK issue.
# https://github.com/pyvista/pyvista/issues/147

plotter.show_grid(font_size=20)

plotter.show(screenshot=conc_png)
