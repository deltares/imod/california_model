import pandas as pd

####SETTINGS####
modelname = "SF_Bay"
model_extent = "data/1-external/extent/sf_bay.shp"

START = "2012-09-01"  # Start date of meteorological data fetched from server
END = "2020-09-01"  # End date of meteorological data fetched from server

proj_epsg = "epsg:26910"  # EPSG code of the projection of the model

dcell = 1000.0  # Cell size in meters
n_layers = 30  # Number of layers
dt_forcing = 100  # Length of stress period in days
DEM_clipoff_quantile = 0.98  # Quantile beyond which the DEM is clipped.

surface_water_concept = "GHB"

porosity = 0.3  # Porosity
longitudinal_dispersivity = 2.0  # Longitudinal dispersivity in meters

solver = "pcg"
hclose = 0.005  # Closure criterium for heads

scenarios = [1, 2, 3]  # Scenario numbers to simulate, see config/scenarios.csv

####CREATE VALUES TO FILL WILDCARDS#####
TIMES = pd.date_range(start=START, end=END, freq="A")
TIMES = TIMES.strftime("%Y-%m-%d")

DFM_numbers = range(0, 16)

BDG_PKGS = ["drn", "rch", "sto"]
BDG_PKGS.append(surface_water_concept.lower())


####End result####
rule all:
    input:
        expand("reports/figures/conc_end_{scenario}.png", scenario=scenarios),
        expand("reports/figures/head_end_{scenario}.png", scenario=scenarios),
        expand("reports/figures/waterbalance_{scenario}.png", scenario=scenarios),
        expand(
            "reports/figures/scenario_{scenario}/heads_comparison", scenario=scenarios
        ),


####BUILD#####
rule create_all_models:
    input:
        expand(
            "data/4-output/scenario_{scenario}/%s.run" % modelname, scenario=scenarios
        ),


rule create_model:
    input:
        path_riv=r"data/2-interim/pkgs/riv_{scenario}.nc",
        path_ghb=r"data/2-interim/pkgs/ghb_{scenario}.nc",
        path_lpf=r"data/2-interim/pkgs/lpf_{scenario}.nc",
        path_bas=r"data/2-interim/pkgs/bas.nc",
        path_btn=r"data/2-interim/pkgs/btn.nc",
        path_rch=r"data/2-interim/pkgs/rch_{scenario}.nc",
        path_drn=r"data/2-interim/pkgs/drn_{scenario}.nc",
        script="src/2-build/create_model.py",
    group:
        "scenarios"
    params:
        modelname=modelname,
        longitudinal_dispersivity=longitudinal_dispersivity,
        dcell=dcell,
        hclose=hclose,
        solver=solver,
        surface_water_concept=surface_water_concept,
        proj_epsg=proj_epsg,
        scenario_nr="{scenario}",
    output:
        path_runf="data/4-output/scenario_{scenario}/%s.run" % modelname,
    script:
        "src/2-build/create_model.py"


####RUN####
rule run_model:
    input:
        r"data/4-output/scenario_{scenario}/%s.run" % modelname,
    group:
        "scenarios"
    output:
        temp(directory("data/4-output/scenario_{scenario}/conc")),
        temp(directory("data/4-output/scenario_{scenario}/head")),
        temp(
            directory(
                expand(
                    "data/4-output/scenario_{{scenario}}/bdg{bdg_pkg}", bdg_pkg=BDG_PKGS
                )
            )
        ),
        #bdg to keep
        temp(
            directory(
                expand(
                    "data/4-output/scenario_{{scenario}}/bdg{bdg_pkg}",
                    bdg_pkg=["bnd", "fff", "flf", "frf"],
                )
            )
        ),
        #bdg to remove
        temp(directory("data/4-output/scenario_{scenario}/dcdt")),
        temp(directory("data/4-output/scenario_{scenario}/imod-wq_tmp")),
    shell:
        r".\src\3-model\run_model.bat data\4-output\scenario_{wildcards.scenario}> .\data\4-output\scenario_{wildcards.scenario}\std.out"


####VISUALIZE####
rule plot_concs:
    input:
        conc_nc="data/4-output/scenario_{scenario}/conc.nc",
        script="src/5-visualize/visualize_concentrations.py",
    output:
        conc_png="reports/figures/conc_end_{scenario}.png",
    group:
        "scenarios"
    script:
        "src/5-visualize/visualize_concentrations.py"


rule plot_heads:
    input:
        head_nc="data/4-output/scenario_{scenario}/head.nc",
        script="src/5-visualize/visualize_heads.py",
    output:
        head_png="reports/figures/head_end_{scenario}.png",
    group:
        "scenarios"
    script:
        "src/5-visualize/visualize_heads.py"


rule plot_waterbalance:
    input:
        bdg_nc="data/4-output/scenario_{scenario}/bdg.nc",
        script="src/5-visualize/plot_sankey.py",
    output:
        sankey_png="reports/figures/waterbalance_{scenario}.png",
    group:
        "scenarios"
    script:
        "src/5-visualize/plot_sankey.py"


rule heads_comparison:
    input:
        head_nc="data/4-output/scenario_{scenario}/head.nc",
        obs_csv="data/2-interim/head_observations.csv",
        script="src/4-analyze/heads_comparison.py",
    output:
        figure_dir=directory("reports/figures/scenario_{scenario}/heads_comparison"),
    group:
        "scenarios"
    script:
        "src/4-analyze/heads_comparison.py"


####POST-PROCESS####
rule idf_to_netcdf:
    input:
        bdg_dirs=expand(
            "data/4-output/scenario_{{scenario}}/bdg{bdg_pkg}", bdg_pkg=BDG_PKGS
        ),
        head_dir="data/4-output/scenario_{scenario}/head",
        conc_dir="data/4-output/scenario_{scenario}/conc",
        script="src/4-analyze/idf_to_netcdf.py",
    group:
        "scenarios"
    params:
        proj_epsg=proj_epsg,
    output:
        bdg_nc="data/4-output/scenario_{scenario}/bdg.nc",
        head_nc="data/4-output/scenario_{scenario}/head.nc",
        conc_nc="data/4-output/scenario_{scenario}/conc.nc",
    script:
        "src/4-analyze/idf_to_netcdf.py"


####SETUP####
rule download_precipitation:
    output:
        paths_prcp=protected(
            expand("data/1-external/meteoric_forcing/PRCP_{times}.nc", times=TIMES[:-1])
        ),
    params:
        time_strings=TIMES,
    script:
        "src/0-setup/download_meteorology.py"


# NOTE! Reading data from the P: drive is very slow, better to run this part on a Horizon node
# Snakemake does not recognize absolute paths, the absolute path below is just to save the path to
# data source somehow.
rule select_DFM_output:
    output:
        paths_DFM_sel=protected(
            expand(
                "data/1-external/DFM_SELECT/r12_{number:04}_map.nc", number=DFM_numbers
            )
        ),
    script:
        "src/0-setup/select_DFM_output.py"


#    input:
#        paths_DFM = expand(r"p:\1210188-baydelta\temperatureruns_final\run116amriv55422THadv_strDW\DFM_OUTPUT_r12\r12_{number:04}_map.nc", number=DFM_numbers)


# Upscale DEM from 10m to 100m beforehand for speedup in demos
rule upscale_DEM:
    input:
        path_DEM_original="data/1-external/DEM/sfbaydeltadem10m2016.asc",
    output:
        path_DEM_upscaled=protected(
            "data/1-external/DEM_100m/sfbaydeltadem100m2016.asc"
        ),
    script:
        "src/0-setup/upscale_original_DEM.py"


rule compress_DFM_output:
    input:
        paths_DFM_sel=expand(
            "data/1-external/DFM_SELECT/r12_{number:04}_map.nc", number=DFM_numbers
        ),
    output:
        paths_DFM_comp=protected(
            expand(
                "data/1-external/DFM_compressed/r12_{number:04}_map.nc",
                number=DFM_numbers,
            )
        ),
    script:
        "src/0-setup/compress_DFM_output"


rule compress_meteo:
    input:
        paths_prcp=expand(
            "data/1-external/meteoric_forcing/PRCP_{times}.nc", times=TIMES[:-1]
        ),
    output:
        paths_prcp_compressed=protected(
            expand(
                "data/1-external/meteoric_compressed/PRCP_{times}.nc", times=TIMES[:-1]
            )
        ),
    script:
        "src/0-setup/compress_meteorology"


####PREPARE####
# -----Scenarios-----#
rule unpack_scenarios:
    input:
        scenarios_csv="config/scenarios.csv",
    output:
        scenario_files=expand(
            "data/2-interim/scenarios/params_{scenario}.csv", scenario=scenarios
        ),
    params:
        scenario_nrs=scenarios,
    script:
        "src/1-prepare/unpack_scenarios.py"


# -----Observations-----#
rule adapt_head_observations:
    input:
        model_grid_3d_nc="data/2-interim/model_grid_3d.nc",
        dem_nc="data/2-interim/dem_model.nc",
        obs_raw=(
            "data/1-external/observations/groundwater_discrete_acwd_data_xy_nad83.csv"
        ),
        script="src/1-prepare/head_observation_adapter.py",
    output:
        obs_csv="data/2-interim/head_observations.csv",
    script:
        "src/1-prepare/head_observation_adapter.py"


# -----Geometry------#
rule create_2d_grid:
    input:
        path_extent=model_extent,
        script="src/1-prepare/create_2d_grid.py",
    output:
        path_model_grid_2d="data/2-interim/model_grid_2d.nc",
    params:
        dcell=dcell,
        proj_epsg=proj_epsg,
    script:
        "src/1-prepare/create_2d_grid.py"


rule prepare_2d_thickness:
    input:
        path_transmissivity="data/1-external/transmissivity_USA/deGraaf2020_transmissivity_totalaquifer.map",
        path_thickness=(
            "data/1-external/transmissivity_USA/total_thickness_aquifers_1.map"
        ),
        path_extent=model_extent,
        path_model_grid_2d="data/2-interim/model_grid_2d.nc",
        script="src/1-prepare/prepare_2d_thickness.py",
    output:
        path_geology_2d="data/2-interim/geology_2d.nc",
    params:
        proj_epsg=proj_epsg,
    script:
        "src/1-prepare/prepare_2d_thickness.py"


rule prepare_DEM:
    input:
        path_DEM="data/1-external/DEM_100m/sfbaydeltadem100m2016.asc",
        path_geology_2d="data/2-interim/geology_2d.nc",
    output:
        path_DEM_model="data/2-interim/dem_model.nc",
    priority: 50
    params:
        proj_epsg=proj_epsg,
    script:
        "src/1-prepare/prepare_DEM.py"


rule create_3d_grid:
    input:
        path_DEM_model="data/2-interim/dem_model.nc",
        path_geology_2d="data/2-interim/geology_2d.nc",
        script="src/1-prepare/create_3d_grid.py",
    output:
        path_model_grid_3d="data/2-interim/model_grid_3d.nc",
    params:
        nlay=n_layers,
        DEM_clipoff_quantile=DEM_clipoff_quantile,
    script:
        "src/1-prepare/create_3d_grid.py"


rule create_geology:
    input:
        path_geology_2d="data/2-interim/geology_2d.nc",
        path_model_grid_3d="data/2-interim/model_grid_3d.nc",
        scenario_file="data/2-interim/scenarios/params_{scenario}.csv",
        script="src/1-prepare/create_geology.py",
    group:
        "scenarios"
    output:
        path_geology_3d="data/2-interim/geology_3d_{scenario}.nc",
    script:
        "src/1-prepare/create_geology.py"


# ------Boundary Conditions-------#
rule prepare_rch:
    input:
        path_geology_2d="data/2-interim/geology_2d.nc",
        paths_prcp=expand(
            "data/1-external/meteoric_compressed/PRCP_{times}.nc", times=TIMES[:-1]
        ),
        script="src/1-prepare/prepare_rch.py",
    output:
        path_recharge="data/2-interim/recharge.nc",
    params:
        proj_epsg=proj_epsg,
        dt_forcing=dt_forcing,
    script:
        "src/1-prepare/prepare_rch.py"


rule dflow_fm_to_shp:
    input:
        paths_DFM=expand(
            "data/1-external/DFM_compressed/r12_{number:04}_map.nc", number=DFM_numbers
        ),
        script="src/1-prepare/dflow_fm_to_shp.py",
    output:
        paths_river_shp=expand(
            "data/2-interim/river_shapes/river_shape_{number:04}.shp",
            number=DFM_numbers,
        ),
    params:
        proj_epsg=proj_epsg,
    script:
        "src/1-prepare/dflow_fm_to_shp.py"


rule resample_time_river:
    input:
        paths_DFM=expand(
            "data/1-external/DFM_compressed/r12_{number:04}_map.nc", number=DFM_numbers
        ),
    output:
        paths_river_nc=expand(
            "data/2-interim/river_ncs/r12_{number:04}_map.nc", number=DFM_numbers
        ),
    params:
        dt_forcing=dt_forcing,
    script:
        "src/1-prepare/resample_time_river.py"


rule rasterrize_river_shp:
    input:
        paths_river_shp=expand(
            "data/2-interim/river_shapes/river_shape_{number:04}.shp",
            number=DFM_numbers,
        ),
        paths_river_nc=expand(
            "data/2-interim/river_ncs/r12_{number:04}_map.nc", number=DFM_numbers
        ),
        path_geology_2d="data/2-interim/geology_2d.nc",
        script="src/1-prepare/rasterrize_river_shp.py",
    output:
        path_river_grid="data/2-interim/river_grid.nc",
    script:
        "src/1-prepare/rasterrize_river_shp.py"


rule create_land_mask:
    input:
        path_model_grid_3d="data/2-interim/model_grid_3d.nc",
        path_river_grid="data/2-interim/river_grid.nc",
    output:
        path_land_mask="data/2-interim/land_mask.nc",
    script:
        "src/1-prepare/create_land_mask.py"


rule repeat_year_river:
    input:
        path_river="data/2-interim/river_grid.nc",
        path_time_model="data/2-interim/recharge.nc",
        script="src/1-prepare/repeat_year_river.py",
    output:
        path_river_nyears="data/2-interim/river_grid_nyears.nc",
    params:
        dt_forcing=dt_forcing,
    script:
        "src/1-prepare/repeat_year_river.py"


# --------Initial Conditions-------#
rule create_initial_head:
    input:
        path_model_grid_3d="data/2-interim/model_grid_3d.nc",
        path_river_grid="data/2-interim/river_grid.nc",
        script="src/1-prepare/estimate_initial_head.py",
    output:
        path_initial_head="data/2-interim/initial_head.nc",
    script:
        "src/1-prepare/estimate_initial_head.py"


rule create_initial_salt:
    input:
        path_river_grid="data/2-interim/river_grid.nc",
        path_model_grid_3d="data/2-interim/model_grid_3d.nc",
        script="src/1-prepare/estimate_initial_salt.py",
    output:
        path_initial_salt="data/2-interim/initial_salt.nc",
    script:
        "src/1-prepare/estimate_initial_salt.py"


# -----Create Package inputs------#
rule create_BAS:
    input:
        path_model_grid_3d="data/2-interim/model_grid_3d.nc",
        path_initial_head="data/2-interim/initial_head.nc",
    output:
        path_bas=r"data/2-interim/pkgs/bas.nc",
    script:
        "src/1-prepare/pkg_BAS.py"


rule create_BTN:
    input:
        path_model_grid_3d="data/2-interim/model_grid_3d.nc",
        path_initial_salt="data/2-interim/initial_salt.nc",
        script="src/1-prepare/pkg_BTN.py",
    output:
        path_btn=r"data/2-interim/pkgs/btn.nc",
    params:
        porosity=porosity,
    script:
        "src/1-prepare/pkg_BTN.py"


rule create_LPF:
    input:
        path_geology_3d="data/2-interim/geology_3d_{scenario}.nc",
    group:
        "scenarios"
    output:
        path_lpf=r"data/2-interim/pkgs/lpf_{scenario}.nc",
    script:
        "src/1-prepare/pkg_LPF.py"


rule create_RIV:
    input:
        path_river="data/2-interim/river_grid_nyears.nc",
        path_model_grid_3d="data/2-interim/model_grid_3d.nc",
        scenario_file="data/2-interim/scenarios/params_{scenario}.csv",
        script="src/1-prepare/pkg_RIV.py",
    output:
        path_pkg_riv=r"data/2-interim/pkgs/riv_{scenario}.nc",
    group:
        "scenarios"
    params:
        dcell=dcell,
    script:
        "src/1-prepare/pkg_RIV.py"


rule create_GHB:
    input:
        path_pkg_riv=r"data/2-interim/pkgs/riv_{scenario}.nc",
    output:
        path_pkg_ghb=r"data/2-interim/pkgs/ghb_{scenario}.nc",
    group:
        "scenarios"
    script:
        "src/1-prepare/pkg_GHB.py"


rule create_DRN:
    input:
        path_land_mask="data/2-interim/land_mask.nc",
        path_model_grid_3d="data/2-interim/model_grid_3d.nc",
        scenario_file="data/2-interim/scenarios/params_{scenario}.csv",
        script="src/1-prepare/pkg_DRN.py",
    group:
        "scenarios"
    output:
        path_pkg_drn=r"data/2-interim/pkgs/drn_{scenario}.nc",
    params:
        dcell=dcell,
    script:
        "src/1-prepare/pkg_DRN.py"


rule create_RCH:
    input:
        path_recharge="data/2-interim/recharge.nc",
        path_land_mask="data/2-interim/land_mask.nc",
        scenario_file="data/2-interim/scenarios/params_{scenario}.csv",
        script="src/1-prepare/pkg_RCH.py",
    group:
        "scenarios"
    output:
        path_rch=r"data/2-interim/pkgs/rch_{scenario}.nc",
    script:
        "src/1-prepare/pkg_RCH.py"
