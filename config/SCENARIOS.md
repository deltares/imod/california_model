Scenarios
============================
This document describes parameters that can be configured in ./config/scenarios.csv

 * **resistance_river**: Resistance of river bed in days
 * **resistance_drain**: Resistance of drains in days
 * **precipitation_reduction**: Reduction factor of how much precipitation enters the groundwater system 
 * **kh_multiplication**: Multiplication factor for horizontal hydraulic conductivities, e.g. for simple sensitivity tests
 * **anisotropy**: Anisotropy factor for the hydraulic conductivity, kv = kh / anisotropy