Model-o-matic: Groundwater model for California
==============================

Quickly set up a variable-density flow groundwater model of an area in California
in a reproducible workflow. The aim of this repository is to quickly and efficiently
provide a first, reproducible starting model to kick off a project and start
discussing model concepts, instead of spending time on data handling.  

This readme describes how to get the workflow to work, for the assumptions
made in the workflow we refer to the [technical document](TECHNICAL.md).

Data
----
The external data required to kick off the project is available using the DVC commands described below. 

The sources of the data can be found in the links below (you do not have to
download these yourself to run the workflow):

* The [Digital_Elevation_Map](https://www.sciencebase.gov/catalog/item/58599681e4b01224f329b484) of Fregoso et al. (2017)

* The [Aquifer_Transmissivity_and_Thickness](https://datacommons.cyverse.org/browse/iplant/home/shared/commons_repo/curated/deGraaf_hyper-resolution_parameterizition) data of the USA of de Graaf et al. (2020), at 1 km.

* Downloaded the [precipitation](http://thredds.cencoos.org/thredds/catalog.html?dataset=COAMPS_4KM_TTL_PRCP) data (there is also a script to download the precipitation data with a chosen time and extent.)

Installation
============

Manual installation pt1
-----------------------
1. Install [git](https://git-scm.com/downloads) 
2. Install [dvc](https://dvc.org)


Cloning the repository
----------------------
We are cloning the repository from Gitlab with git as follows:

```console
git clone https://gitlab.com/deltares/imod/california_model.git
```

(Note that pushing again to this repo then requires a 
[personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html))

Fetching data
-------------

We'll fetch the data with [dvc](https://dvc.org/), 
which is installed with mamba.
In order to fetch all data with dvc (note this will require a google 
account to access), call:

```console
dvc pull -r google_drive
```

or if connected to the Deltares intranet, you can call, 
but this a lot slower as data has to be transferred via the VPN:

```console
dvc pull -r blue_earth
```

Manual installation pt2
-----------------------

You can find the ``bin/Deltaforge-v0.3.1.exe``, run this executable and click
through the wizard to install Deltaforge. This will install a python
environment, which can be activated by starting the **Deltaforge prompt**.

Workflow
========

[Snakemake](https://snakemake.readthedocs.io/en/stable/) is used as the 
workflow manager and makes the workflow reproducible. 
The workflow is located in the ```Snakefile``` in the repository. Here, 
also some settings can be set. 

Starting the Shell
------------------

Start the **Deltaforge prompt** by pressing the *Windows* key (or clicking
*"Start"*) and typing *"Deltaforge prompt"*.

Running the workflow
--------------------

To run the workflow, first `cd` into the repository, and then run:

```console
snakemake
```

This will run three model scenarios, 
defined in `config/scenarios.csv`, sequentially. 
This will take ~15 minutes. 

You can run multiple scenarios in parallel by providing 
the `--cores` option to snakemake. 
For example, to use three cores you can call:

```console
snakemake --cores 3
```

Notice that snakemake will also automatically run independent jobs,
not just scenarios, in parallel!

In addition, say you updated certain input data, e.g. 
`model_extent.shp`, 
you can rerun every script (directly or indirectly) depending on 
that data by calling:

```console
snakemake -R model_extent.shp
```

Results
-------
If everything worked accordingly, plots of the results can be 
found under `./reports/figures/`.

This is an example end result of the workflow for the San Francisco Bay:
![conc](./docs/conc_end.png)

Viewing the workflow
--------------------
To update the workflow figure in the repository, you first have to call once:

```console
dot -c
```

and then you can call:

```console
snakemake --rulegraph | dot -Tpng > docs/dag.png
```

The workflow currently looks as follows:
![dag](./docs/dag.png)


Known issues
============
Quite often `snakemake --rulegraph | dot -Tpng > docs/dag.png` 
creates corrupted files on Windows. 
This is because Graphviz apparently cannot deal with 
the Windows-style line-endings (CRLF)
I found two workarounds for this.
1. Either calling:
`snakemake --rulegraph | dot -Tsvg > docs/dag.svg`,
opening the `.svg` file in [Inkscape](https://inkscape.org/), 
and exporting this to a `.png`. 
2. Or installing `git` in the `modelamatic` conda environment, 
this also installs a tool called `dos2unix`, 
which `git` uses to convert CRLF to LF.
We can use this to convert CRLF to LF before calling Graphviz' `dot`.
This also can fix other issues `dot` might throw at you on Windows.

```console
mamba install git
snakemake --rulegraph | dos2unix | dot -Tpng > docs/dag.png
```


Project Organization
====================

    .
    ├── AUTHORS.md
    ├── LICENSE
    ├── README.md
    ├── bin                 <- Your compiled model code can be stored here (not tracked by git)
    ├── config              <- Configuration files, e.g., for doxygen or for your model if needed
    ├── data                
    │   ├── 1-external      <- Data external to the project.
    │   ├── 2-interim       <- Intermediate data that has been altered.
    │   ├── 3-input         <- The processed data sets, ready for modeling.
    │   ├── 4-output        <- Data dump from the model.
    │   └── 5-visualization <- Post-processed data, ready for visualisation.
    ├── docs                <- Documentation, e.g., doxygen or scientific papers (not tracked by git)
    ├── notebooks           <- Jupyter notebooks
    ├── reports             <- For a manuscript source, e.g., LaTeX, Markdown, etc., or any project reports
    │   └── figures         <- Figures for the manuscript or reports
    └── src                 <- Source code for this project
        ├── 0-setup         <- Install necessary software, dependencies, pull other git projects, etc.
        ├── 1-prepare       <- Scripts and programs to process data, from 1-external to 2-interim.
        ├── 2-build         <- Scripts to create model specific inputm from 2-interim to 3-input. 
        ├── 3-model         <- Scripts to run model and convert or compress model results, from 3-input to 4-output.
        ├── 4-analyze       <- Scripts to post-process model results, from 4-output to 5-visualization.
        └── 5-visualize     <- Scripts for visualisation of your results, from 5-visualization to ./report/figures.