Assumptions made in workflow
============================
This document describes the important conceptualization and regridding choices that 
are made in  this workflow as some choices are already made for the 
user in this workflow. Mind that the goal is to quickly set up models, if more 
additional processes are required, it has to be modified by the user self.

Geometry
--------
The horizontal geometry is created with a polygon that is specified by the 
user, and converted into a three-dimensional geometry with the help of an 
US covering [aquifer thickness dataset](https://datacommons.cyverse.org/browse/iplant/home/shared/commons_repo/curated/deGraaf_hyper-resolution_parameterizition). 
This dataset is also used to remove parts of the model where the aquifer thickness
equals zero, because bedrock is outcropping there.

Discretization
--------------
Horizontal discretization is set by providing a cellsize. This is then regular
discretized into a user specified amount of layers. The cell thickness is unknown 
beforehand, since the aquifer thickness dataset has to provide the vertical extent of 
the model domain. Therefore, it was decided to let the user specify an amount of layers,
to prevent unexpected large files. 

Surface waters
--------------
DFLOW-FM data should be provided in a NetCDF following the [UGRID conventions](https://ugrid-conventions.github.io/ugrid-conventions/). 
Currently the workflow uses a nearest-neighbour lookup to rasterrize 
the DFLOW-FM meshes. Using very fine groundwater model rasters, this 
might result in clear jumps in values and some additional smoothing of 
the raster might be necessary afterwards.

The rasterrized DFLOW-FM output can be used to provide boundary conditions
using two different conceptualizations: Either as General Head Boundary ("GHB"),
or as a River ("RIV") package for iMOD-WQ. The difference between these two is 
that the river package is slightly more advanced, as it limits river
infiltration when groundwater levels are lower than the river bottom. This
is thus especially important to systems with deep groundwater tables. However, 
this introduces more non-linearity in the mathmatical problem to be solved,
which can lead to problems with numerical convergence. Therefore, for systems
with deep bathymetries, for example bays and seas, the user is adviced to use
the GHB package.

Recharge
--------
[Precipitation](http://thredds.cencoos.org/thredds/catalog.html?dataset=COAMPS_4KM_TTL_PRCP) is downloaded and regridded into model grid using 
a nearest neighbour lookup.

The model features no unsaturated zone, moreover evapotranspiration is not 
incorporated, instead a (constant) precipitation reduction factor should be set 
by the user in the [scenario manager](./config/SCENARIO.md), 
which is multiplied with the precipitation to calculate a recharge. 
These assumptions can result in errors in calculating groundwater
dynamics in dry areas, where groundwater tables are deep.

[The thredds server](http://thredds.cencoos.org/thredds/catalog.html) does provide
the data to calculate the Penman-Monteith evapotranspirative flux, but since
this involves  downloading and handling of quite some extra files, it falls 
outside of the scope of this project. The workflow could, however, easily be 
extended with it. 

Properly modelling the unsaturated zone requires either the UZF package in MODFLOW,
or a coupling with seperate model codes, which is outside the scope of this project.

Overland flow
-------------
"Drains" are installed in every cell at surface level to prevent groundwater levels
exceeding surface level. Outflow via these drains can be seen as Dunnian overland
flow. No "surface ponding" is taken into account. If these ponds are deemed 
important, surface ponds can be provided by the user as "river".

Subsurface
----------
Hydraulic conductivities are estimated from the transmissivities and 
thicknesses provided by this [US subsurface dataset](https://datacommons.cyverse.org/browse/iplant/home/shared/commons_repo/curated/deGraaf_hyper-resolution_parameterizition). 
Therefore, there currently is no variation of the hydraulic conductivity in the vertical direction. 
Experience has shown that the resulting hydraulic conductivity of this 
dataset is not always optimal, so the user can set a multiplication factor 
and a anisotropy for the hydraulic conductivity in the 
[scenario manager](./config/SCENARIO.md),
to allow a better match with observations.

Initial conditions
------------------
The workflow assumes that groundwater levels are initially at surface level, 
therefore the model requires some spin-up time to drain this excess groundwater.
Furthermore, initial salinity concentrations are calculated using the
Ghijben-Herzberg approximation, for which we use the initial groundwater level
and salinity of the closes surface water cell at t=0 as initial salinity. This
results in a relatively stable initial fresh-salt distribution, but, since
solute transport is very slow in groundwater, will still require a significant 
spin-up time, of at least a decade.